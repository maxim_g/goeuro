//
//  GoEuroTests.m
//  GoEuroTests
//
//  Created by Max on 16.12.16.
//  Copyright © 2016 Max G. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "DataProvider.h"

@interface GoEuroTests : XCTestCase

@end

@implementation GoEuroTests

- (void)setUp
{
    [super setUp];
}

- (void)testBuses
{
    XCTestExpectation *expectation = [self expectationWithDescription:@"getBuses"];
    [DataProvider getItemsWithType:TravelTypeBus completionHandler:^(NSArray *result) {
        XCTAssertNotNil(result);
        XCTAssert(result.count > 0);
        
        NSDictionary *item = result[0];
        XCTAssert([item[@"price_in_euros"] isKindOfClass:[NSNumber class]]);
        XCTAssert([item[@"provider_logo"] isKindOfClass:[NSString class]]);
        XCTAssert([item[@"departure_time"] isKindOfClass:[NSString class]]);
        XCTAssert([item[@"arrival_time"] isKindOfClass:[NSString class]]);
        XCTAssert([item[@"number_of_stops"] isKindOfClass:[NSNumber class]]);
        
        [expectation fulfill];
    }];
    
    [self waitForExpectationsWithTimeout:INT16_MAX handler:^(NSError *error) {
        XCTAssert(!error, @"data provider did not return any data:\t%@", error);
    }];
}

- (void)testTrains
{
    XCTestExpectation *expectation = [self expectationWithDescription:@"getTrains"];
    [DataProvider getItemsWithType:TravelTypeTrain completionHandler:^(NSArray *result) {
        XCTAssertNotNil(result);
        XCTAssert(result.count > 0);
        
        NSDictionary *item = result[0];
        XCTAssert([item[@"price_in_euros"] isKindOfClass:[NSNumber class]]);
        XCTAssert([item[@"provider_logo"] isKindOfClass:[NSString class]]);
        XCTAssert([item[@"departure_time"] isKindOfClass:[NSString class]]);
        XCTAssert([item[@"arrival_time"] isKindOfClass:[NSString class]]);
        XCTAssert([item[@"number_of_stops"] isKindOfClass:[NSNumber class]]);
        
        [expectation fulfill];
    }];
    
    [self waitForExpectationsWithTimeout:INT16_MAX handler:^(NSError *error) {
        XCTAssert(!error, @"data provider did not return any data:\t%@", error);
    }];
}

- (void)testFlights
{
    XCTestExpectation *expectation = [self expectationWithDescription:@"getFlights"];
    [DataProvider getItemsWithType:TravelTypeFlight completionHandler:^(NSArray *result) {
        XCTAssertNotNil(result);
        XCTAssert(result.count > 0);
        
        NSDictionary *item = result[0];
        XCTAssert([item[@"price_in_euros"] isKindOfClass:[NSNumber class]]);
        XCTAssert([item[@"provider_logo"] isKindOfClass:[NSString class]]);
        XCTAssert([item[@"departure_time"] isKindOfClass:[NSString class]]);
        XCTAssert([item[@"arrival_time"] isKindOfClass:[NSString class]]);
        XCTAssert([item[@"number_of_stops"] isKindOfClass:[NSNumber class]]);
        
        [expectation fulfill];
    }];
    
    [self waitForExpectationsWithTimeout:INT16_MAX handler:^(NSError *error) {
        XCTAssert(!error, @"data provider did not return any data:\t%@", error);
    }];
}

@end
