//
//  ViewController.m
//  GoEuro
//
//  Created by Max on 16.12.16.
//  Copyright © 2016 Max G. All rights reserved.
//

#import "MainViewController.h"
#import "TableViewCell.h"
#import "DataProvider.h"

NSString * const kTrains = @"trains";
NSString * const kFlights = @"flights";
NSString * const kBuses = @"buses";

@interface MainViewController () <UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, weak) IBOutlet UITableView *tableView;

@property (nonatomic, strong) NSDictionary *items;
@property (nonatomic, assign) TravelType currentTravelType;
@end

@implementation MainViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self prepareData];
    [self getTraveInfo];
}

#pragma mark - IBActions

- (IBAction)sortPressed:(id)sender
{
    static BOOL ascending = NO;
    
    NSMutableArray *sortedArray = [NSMutableArray arrayWithArray:self.items[self.currentTravelTypeKey]];
    NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"objID" ascending:ascending];
    [sortedArray sortUsingDescriptors:@[descriptor]];
    
    [self updateTravelInfoWithResult:sortedArray.copy];
    
    ascending = !ascending;
}

- (IBAction)travelTypeChanged:(UISegmentedControl *)sender
{
    self.currentTravelType = (TravelType)sender.selectedSegmentIndex;
    
    [self updateTravelInfoWithResult:@[]];
    [self getTraveInfo];
}

#pragma mark - Private Methods

- (void)prepareData
{
    self.items = @{kTrains  : @[],
                   kFlights : @[],
                   kBuses   : @[]};
    
    self.currentTravelType = TravelTypeBus;
}

- (void)getTraveInfo
{
    [DataProvider getItemsWithType:self.currentTravelType completionHandler:^(NSArray *result) {
        [self updateTravelInfoWithResult:result];
    }];
}

- (void)updateTravelInfoWithResult:(NSArray *)result
{
    NSMutableDictionary *items = self.items.mutableCopy;
    items[self.currentTravelTypeKey] = result;
    self.items = items;
    
    [self.tableView reloadData];
}

- (NSString *)currentTravelTypeKey
{
    return self.currentTravelType == TravelTypeBus ? kBuses : (self.currentTravelType == TravelTypeTrain ? kTrains : kTrains);
}
     
#pragma mark - TableView DataSource & Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.items[self.currentTravelTypeKey] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TravelTableViewCell"];
    [cell configureWithModel:self.items[self.currentTravelTypeKey][indexPath.row]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    //for iOS 7 should be used UIAlertView
    UIAlertController *alertConstroller = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"", nil)
                                                                              message:NSLocalizedString(@"Offer details are not yet implemented!", nil)
                                                                       preferredStyle:UIAlertControllerStyleAlert];
    [alertConstroller addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil)
                                                         style:UIAlertActionStyleCancel handler:nil]];
    [self presentViewController:alertConstroller animated:YES completion:nil];
}

@end
