//
//  TableViewCell.m
//  GoEuro
//
//  Created by Max on 08.01.17.
//  Copyright © 2017 Max G. All rights reserved.
//

#import "TableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface TableViewCell ()
@property (nonatomic, weak) IBOutlet UIImageView *logoImageView;
@property (nonatomic, weak) IBOutlet UILabel *priceLabel;
@property (nonatomic, weak) IBOutlet UILabel *timingLabel;
@property (nonatomic, weak) IBOutlet UILabel *travelTimeLabel;
@property (nonatomic, weak) IBOutlet UILabel *stopsNumberLabel;

@property (nonatomic, strong) TravelModel *model;
@end

@implementation TableViewCell

- (void)prepareForReuse
{
    [super prepareForReuse];
    
    self.priceLabel.attributedText = nil;
    self.stopsNumberLabel.text = nil;
    self.travelTimeLabel.text = nil;
    self.timingLabel.text = nil;
    self.logoImageView.image = nil;
}

- (void)configureWithModel:(TravelModel *)model
{
    self.model = model;
    
    self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    self.priceLabel.attributedText = self.attributedPriceText;
    self.stopsNumberLabel.text = self.stopsNumberText;
    self.travelTimeLabel.text = self.travelTimeText;
    self.timingLabel.text = self.timimgText;
    [self updateLogoImageView];
    
}

#pragma mark - Private Methods

- (void)updateLogoImageView
{
    [self.logoImageView sd_setImageWithURL:self.model.logoURL
                                 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                     //update cell after image is loaded
                                     [self setNeedsLayout];
                                 }];
}

- (NSAttributedString *)attributedPriceText
{
    NSString *price = [NSString stringWithFormat:@"%.2f", self.model.price.floatValue];
    NSMutableAttributedString *result = [[NSMutableAttributedString alloc] initWithString:[@"€" stringByAppendingString:price]];
    [result addAttribute:NSFontAttributeName
                   value:[UIFont systemFontOfSize:15.0]
                   range:NSMakeRange(0, price.length - 2)];
    [result addAttribute:NSFontAttributeName
                   value:[UIFont systemFontOfSize:12.0]
                   range:NSMakeRange(price.length - 1, 2)];
    
    return result;
}

- (NSString *)timimgText
{
    NSDateFormatter *formatter = [NSDateFormatter new];
    formatter.dateFormat = @"HH:mm";
    
    return [NSString stringWithFormat:@"%@ - %@", [formatter stringFromDate:self.model.departureTime], [formatter stringFromDate:self.model.arrivalTime]];
}

- (NSString *)stopsNumberText
{
    if(!self.model.stopsNumber.intValue) return @"Direct";
    
    NSString *result = self.model.stopsNumber.stringValue;
    
    result = [result stringByAppendingFormat:@" stop"];
    if(self.model.stopsNumber.intValue > 1)
        result = [result stringByAppendingFormat:@"s"];
    
    return result;
}

- (NSString *)travelTimeText
{
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitHour|NSCalendarUnitMinute
                                                 fromDate:self.model.departureTime toDate:self.model.arrivalTime options: 0];
    NSNumber *minutes, *hours = nil;
    hours = @([components hour]);
    minutes = @([components minute]);
    
    return [NSString stringWithFormat:@"%02d:%02dh", hours.intValue, minutes.intValue];
}

@end
