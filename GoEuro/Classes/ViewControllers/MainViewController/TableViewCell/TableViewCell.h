//
//  TableViewCell.h
//  GoEuro
//
//  Created by Max on 08.01.17.
//  Copyright © 2017 Max G. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TravelModel;

@interface TableViewCell : UITableViewCell

- (void)configureWithModel:(TravelModel *)model;

@end
