//
//  TravelModel.swift
//  GoEuro
//
//  Created by Max on 16.12.16.
//  Copyright © 2016 Max G. All rights reserved.
//

import UIKit

@objc
enum TravelKeyType : UInt {
    case ObjID, Logo, Price, DepartureTime, ArrivalTime, StopsNumber
    func stringValue() -> String!
    {
        switch self
        {
            case .ObjID:         return "id"
            case .Logo:          return "provider_logo"
            case .Price:         return "price_in_euros"
            case .DepartureTime: return "departure_time"
            case .ArrivalTime:   return "arrival_time"
            case .StopsNumber:   return "number_of_stops"
        }
    }
}

class TravelModel: NSObject {

    var objID         : NSNumber!
    var logoURL       : URL!
    var price         : NSNumber!
    var departureTime : Date!
    var arrivalTime   : Date!
    var stopsNumber   : NSNumber!
    
    convenience init?(dictionary: Dictionary<String, Any>)
    {
        self.init()
    
        objID = NSNumber.init(value: dictionary[TravelKeyType.ObjID.stringValue()] as! Int64)
        logoURL = URL.init(string: addLogoSize(string: dictionary[TravelKeyType.Logo.stringValue()] as! String))
        stopsNumber = NSNumber.init(value: dictionary[TravelKeyType.StopsNumber.stringValue()] as! UInt)
        departureTime = dateFromString(string: dictionary[TravelKeyType.DepartureTime.stringValue()] as! String)
        arrivalTime = dateFromString(string: dictionary[TravelKeyType.ArrivalTime.stringValue()] as! String)
        guard let priceValue = dictionary[TravelKeyType.Price.stringValue()] as? Float else {
            price = NSNumber.init(value: 0)
            return
        }
        price = NSNumber.init(value: priceValue)
    }
    
    //MARK: Private Functions
    
    private func dateFromString(string: String) -> Date
    {
        let formatter = DateFormatter.init()
        formatter.dateFormat = "HH:mm"
        return formatter.date(from: string)!
    }
    
    private func addLogoSize(string: String) -> String
    {
        return string.replacingOccurrences(of: "{size}", with: "63")
    }
    
}
