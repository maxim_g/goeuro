//
//  DataProvider.m
//  GoEuro
//
//  Created by Max on 08.01.17.
//  Copyright © 2017 Max G. All rights reserved.
//

#import "DataProvider.h"

NSString *const kEndpoint = @"https://api.myjson.com/bins/";

@implementation DataProvider

#pragma mark - Public Methods

+ (void)getItemsWithType:(TravelType)type completionHandler:(CompletionHandler)completionHandler
{
    NSURL *url = [NSURL URLWithString:[kEndpoint stringByAppendingString:[self travelIDWithType:type]]];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        NSArray *data = [self getDataFromURL:url];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if(completionHandler) completionHandler(data);
        });
    });
}

#pragma mark - Private Methods

+ (NSArray*)convertResponseToModels:(NSArray *)array
{
    NSMutableArray *result = @[].mutableCopy;
    
    for(NSDictionary *item in array)
    {
        TravelModel *model = [[TravelModel alloc] initWithDictionary:item];
        [result addObject:model];
    }
    
    return result;
}

+ (NSArray *)getDataFromURL:(NSURL *)url
{
    NSError *error;
    
    NSData *data = [NSData dataWithContentsOfURL:url];
    NSArray *result = [NSJSONSerialization JSONObjectWithData:data
                                                      options:NSJSONReadingMutableContainers
                                                        error:&error];
    
    if(error) return @[];
    
    return result ? [self convertResponseToModels:result] : @[];
}

+ (NSString *)travelIDWithType:(TravelType)type
{
    switch (type) {
        case TravelTypeBus:    return @"37yzm";
        case TravelTypeFlight: return @"w60i";
        case TravelTypeTrain:  return @"3zmcy";
    }
}

@end
