//
//  DataProvider.h
//  GoEuro
//
//  Created by Max on 08.01.17.
//  Copyright © 2017 Max G. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum : NSUInteger {
    TravelTypeTrain,
    TravelTypeBus,
    TravelTypeFlight,
} TravelType;

typedef void (^CompletionHandler)(NSArray * _Nonnull result);

@interface DataProvider : NSObject

+ (void)getItemsWithType:(TravelType)type completionHandler:(_Nullable CompletionHandler)completionHandler;

@end
